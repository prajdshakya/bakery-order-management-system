FROM openjdk:11
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} boms-app.jar
ENTRYPOINT ["java", "-jar", "/boms-app.jar"]