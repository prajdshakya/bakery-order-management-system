package com.developer.boms.serviceImpl;

import com.developer.boms.dto.ResponseDTO;
import com.developer.boms.dto.UsersDTO;
import com.developer.boms.entity.Role;
import com.developer.boms.entity.Users;
import com.developer.boms.projection.UserNameAndIdProjection;
import com.developer.boms.repository.RoleRepo;
import com.developer.boms.repository.UserRepo;
import com.developer.boms.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepo userRepo;
    private final RoleRepo roleRepo;

    public static String bcrypt(String password_plaintext) {
        int workload = 12;
        String salt = BCrypt.gensalt(workload);
        String hashed_password = BCrypt.hashpw(password_plaintext, salt);

        return(hashed_password);
    }

    @Override
    public ResponseDTO saveUser(UsersDTO usersDTO) {
        Users byUsername = userRepo.findByUsername(usersDTO.getUserName());
        if (byUsername!=null) {
            throw new UnsupportedOperationException("username already exists");
        }
        for (Integer rol:usersDTO.getRoles()) {
            if (!roleRepo.findById(rol).isPresent()){
                return new ResponseDTO("role doesnt exists in the system", HttpStatus.BAD_REQUEST, rol);
            }
        }

        String bcryptedPassword = bcrypt(usersDTO.getPassword());
        usersDTO.setPassword(bcryptedPassword);
        Users users=new Users(usersDTO);
        Users savedUser = userRepo.save(users);
        for (int roleId: usersDTO.getRoles()) {
            userRepo.saveRegisterUserRole(savedUser.getId(), roleId);
        }
        return new ResponseDTO("User save successfully", HttpStatus.OK, savedUser);
    }

    @Override
    public ResponseDTO getUsers() {
        List<UserNameAndIdProjection> userList = userRepo.findAllUsers();
        List<Map<String, Object>> responseUserList = new ArrayList<>();
        for (UserNameAndIdProjection userNameAndIdProjection: userList) {
            Map<String, Object> user = new LinkedHashMap<>();
            List<Role> userRoles = roleRepo.findRoleByUserId(userNameAndIdProjection.getId());
            user.put("id", userNameAndIdProjection.getId());
            user.put("userName", userNameAndIdProjection.getUserName());
            user.put("roles", userRoles);
            responseUserList.add(user);
        }
        return new ResponseDTO("list of users and roles", HttpStatus.OK, responseUserList);
    }

    @Override
    public ResponseDTO getUserById(int id) {
        Optional<Users> userById = userRepo.findById(id);
        if (!userById.isPresent()){
            return new ResponseDTO("user does not exist", HttpStatus.NOT_FOUND);
        }
        return new ResponseDTO("user found", HttpStatus.OK, userById.get());
    }


}
