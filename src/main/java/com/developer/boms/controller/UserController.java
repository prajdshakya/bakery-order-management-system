package com.developer.boms.controller;

import com.developer.boms.dto.ResponseDTO;
import com.developer.boms.dto.UsersDTO;
import com.developer.boms.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/user")
@AllArgsConstructor
public class UserController {
    private final UserService userService;

    @GetMapping()
    public ResponseEntity user(){
        ResponseDTO responseDTO = userService.getUsers();
        return new ResponseEntity(responseDTO, responseDTO.getStatus());
    }

    @PostMapping()
    public ResponseEntity saveUser(@RequestBody UsersDTO usersDTO) {
        ResponseDTO responseDTO = userService.saveUser(usersDTO);
        return new ResponseEntity(responseDTO, responseDTO.getStatus());
    }

    @GetMapping("/{id}")
    public ResponseEntity getUserById(@PathVariable("id") int id){
        ResponseDTO responseDTO = userService.getUserById(id);
        return new ResponseEntity(responseDTO, responseDTO.getStatus());
    }
}
