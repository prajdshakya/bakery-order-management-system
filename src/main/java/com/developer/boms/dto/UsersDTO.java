package com.developer.boms.dto;

import lombok.*;

import java.util.List;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UsersDTO {
    private String userName;
    private String password;
    private List<Integer> roles;
}
