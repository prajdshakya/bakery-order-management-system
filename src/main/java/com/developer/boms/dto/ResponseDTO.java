package com.developer.boms.dto;

import lombok.*;
import org.springframework.http.HttpStatus;

@Data
@Getter
@Setter
@NoArgsConstructor
public class ResponseDTO{
    private String message;
    private HttpStatus status;
    private Object data;

    public ResponseDTO(String message, HttpStatus status, Object data) {
        this.message = message;
        this.status = status;
        this.data = data;
    }
    public ResponseDTO(String message, HttpStatus status) {
        this.message = message;
        this.status = status;
    }
}
