package com.developer.boms.service;

import com.developer.boms.dto.ResponseDTO;
import com.developer.boms.dto.UsersDTO;

public interface UserService {
    ResponseDTO saveUser(UsersDTO usersDTO);

    ResponseDTO getUsers();

    ResponseDTO getUserById(int id);
}
