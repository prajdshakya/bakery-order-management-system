package com.developer.boms.projection;

public interface UserNameAndIdProjection {
    int getId();
    String getUserName();

}
